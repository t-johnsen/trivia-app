# trivia-app
A simple quiz application where you can select category of quiz and amount of questions you want.

## Try it out!
[Maris Trivia App]

### Project setup and usage
```
npm install
```

###### Compiles and hot-reloads for development
```
npm run serve
```

###### Compiles and minifies for production
```
npm run build
```

[//]: #
[Maris Trivia App]: <https://maris-trivia-game.netlify.app/>