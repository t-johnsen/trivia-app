import Vue from 'vue';
import VueRouter from 'vue-router';
import StartGame from '../views/StartGame'
import HomePage from '../views/HomePage'
import PlayGame from '../views/PlayGame'
//import TestComp from '../components/TestComp'

Vue.use(VueRouter);

const routes = [
    {
        path: '/',
        component: HomePage,
        name: 'home'
    },
    {
        path: '/start',
        component: StartGame
    },
    {
        path: '/play-game',
        component: PlayGame,
        name: 'PlayGame'
    }
]

const router = new VueRouter({
    routes
})

export default router;